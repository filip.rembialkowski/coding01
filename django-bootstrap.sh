#!/bin/bash

dir=$1
project=$2
app=$3
[ "$dir" -a "$app" ] || { echo "Usage: $0 dir project app" >&2; exit 1; }

echo "### Creating $dir/$project/$app"
mkdir -p $dir/$project/$app

echo "### Bootstrapping project"
django-admin startproject $project $dir

echo "### Bootstrapping app"
django-admin startapp $app $dir/$project/$app

echo "### Django bootstrap done, file listing follows:"
find $dir/manage.py $dir/$project -type f

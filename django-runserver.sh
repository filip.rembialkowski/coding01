#!/bin/bash

dir=$1
project=$2
[ "$dir" -a "$project" ] || { echo "Usage: $0 dir project" >&2; exit 1; }
[ -d "$dir" ] || { echo "Directory [$dir] does not exist" >&2; exit 2; }
[ -e "$dir/manage.py" ] || { echo "[$dir/manage.py] does not exist" >&2; exit 3; }

tmux new-session -s $project -c $dir -d
tmux send-keys "$dir/manage.py makemigrations" Enter
tmux send-keys "$dir/manage.py migrate" Enter
tmux send-keys "$dir/manage.py runserver 0.0.0.0:8000" Enter

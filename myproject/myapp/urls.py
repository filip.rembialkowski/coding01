"""myapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path

from . import views

urlpatterns = [
    # /myapp
    path('', views.index, name='index'),
    # /myapp/users
    path('users/', views.users, name='users'),
    # /myapp/lists
    path('lists/', views.lists, name='lists'),
    # /myapp/user/5
    re_path(r'^user/(?P<user_id>-?\d+)/$', views.user, name='user'),
    # /myapp/list/5
    re_path(r'^list/(?P<list_id>-?\d+)/$', views.list, name='list'),
    # /myapp/list/5/listmod
    re_path(r'^list/(?P<list_id>-?\d+)/listmod/$',
            views.listmod, name='listmod'),
]

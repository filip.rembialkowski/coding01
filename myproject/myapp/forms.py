from django import forms
from django.utils.safestring import mark_safe
from textwrap import dedent
from .models import *


# Django's automatic form (used only for removing users from list).
ListMembersForm = forms.inlineformset_factory(
    List, ListMember, extra=0, fields=['user', 'list'])

# Custom form
class ListModForm(forms.Form):
    def __init__(self, l, *args, **kwargs):
        super(ListModForm, self).__init__(*args, **kwargs)
        self.list = l
        for u in l.nonmembers():
            self.fields[f"user{u.id}"] = forms.BooleanField(
                label=u.fullname(),
                initial=False,
                required=False)

    def save(self):
        for u in self.list.nonmembers():
            if self.cleaned_data[f"user{u.id}"]:
                print(f"Adding user {u} to list {self.list}")
                record = ListMember(user=u, list=self.list)
                record.save()

    def as_html(self):
        """Override HTML generation method.
        """
        output = []
        output.append(dedent(f"""
        <input type="text" id="fltInput" onkeyup="fltFunction()"
            placeholder="Search {len(self.list.nonmembers())} Unassigned Users"
            title="Type in user name">
        <table id="fltList" >"""))
        for u in self.list.nonmembers():
            output.append(dedent(f"""
            <tr class="fltItem">
            <td class="initials"><label for="id_user{u.id}">{u.initials()}</label></td>
              <td class="name_email"><label for="id_user{u.id}">
                <span class="fullname">{u.fullname()}</span>
                <span class="email">{u.email}</span></label></td>
              <td><input type="checkbox" name="user{u.id}" id="id_user{u.id}"></td>
            </tr>"""))
        output.append(dedent("""
        </table>
        <button id="cancel" type="reset">Cancel</button>
        <button id="confirm" type="submit">Confirm</button>
        <script>
        function fltFunction() {
          /* https://www.w3schools.com/howto/howto_js_filter_table.asp */
          var inp, flt, list, item, searchedField, i, txtValue;
          inp = document.getElementById("fltInput");
          flt = inp.value.toUpperCase();
          list = document.getElementById("fltList");
          items = list.getElementsByTagName("tr");
          for (i = 0; i < items.length; i++) {
            searchedField = items[i];
            if (searchedField) {
              txtValue = searchedField.textContent || searchedField.innerText;
              if (txtValue.toUpperCase().indexOf(flt) > -1) {
                items[i].style.display = "";
              } else {
                items[i].style.display = "none";
              }
            }
          }
        }
        </script>"""))

        return mark_safe('\n'.join(output))

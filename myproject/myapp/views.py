from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import get_object_or_404, render
from .models import *
from .forms import *


def index(request):
    return render(request, 'index.html', {'title': 'Home Page'})


def user(request, user_id):
    u = get_object_or_404(User, id=user_id)
    return render(request, 'user.html',
                  {'user': u, 'title': f"User {u}"})


def list(request, list_id):
    l = get_object_or_404(List, id=list_id)
    if request.method == 'POST':
        form = ListMembersForm(instance=l, data=request.POST)
        if form.is_valid():
            data = form.cleaned_data
            print(f"saving ListMembersForm for list {l}")
            form.save()
            return HttpResponseRedirect(reverse('list', args=[list_id]))
        else:
            print("Form is not valid")
    else:
        form = ListMembersForm(instance=l)
    return render(request, 'list.html', {
                  'list': l, 'form': form, 'title': f"List {l.name}"})


def users(request):
    U = User.objects.all()
    return render(request, 'users.html', {'users': U, 'title': 'Users'})


def lists(request):
    L = List.objects.all()
    return render(request, 'lists.html', {
                  'lists': L, 'title': 'Managed Lists'})


def listmod(request, list_id):
    l = get_object_or_404(List, id=list_id)
    if request.method == 'POST':
        form = ListModForm(l, request.POST)
        if form.is_valid():
            data = form.cleaned_data
            print(f"saving ListModForm for list {l}")
            form.save()
            return HttpResponseRedirect(reverse('list', args=[list_id]))
        else:
            print("Form is not valid")
    else:
        form = ListModForm(l)

    return render(request, 'listmod.html', {
                  'list': l, 'list_id': list_id, 'form': form, 'title': f"Assign users to *{l}*"})

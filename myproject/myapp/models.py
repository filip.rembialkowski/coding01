from django.db import models


class Timestamped(models.Model):
    class Meta:
        abstract = True
    date_created = models.DateTimeField(auto_now_add=True, null=False)
    date_modified = models.DateTimeField(auto_now=True, null=False)


class User(Timestamped):
    firstname = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    email = models.EmailField(unique=True, null=False, blank=False)

    def fullname(self):
        if self.firstname and self.lastname:
            return self.firstname + ' ' + self.lastname
        elif self.firstname:
            return self.firstname
        else:
            return self.lastname

    def __str__(self):
        return '{} <{}>'.format(self.fullname(), self.email)

    def initials(self):
        return self.firstname.strip()[0].upper(
        ) + self.lastname.strip()[0].upper()

    def member_of(self):
        return ListMember.objects.filter(user=self)


class List(Timestamped):
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=4000)

    def __str__(self):
        return self.name

    def members(self):
        return ListMember.objects.filter(list=self)

    def member_ids(self):
        return map(lambda m: m.user.id, self.members())

    def nonmembers(self):
        return User.objects.exclude(id__in=self.member_ids())

    def nonmember_ids(self):
        return map(lambda m: m.id, self.nonmembers())


class ListMember(Timestamped):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    list = models.ForeignKey(List, on_delete=models.CASCADE)

from django.contrib import admin

# Register your models here.
from .models import User, List, ListMember

admin.site.register(User)
admin.site.register(List)
admin.site.register(ListMember)
